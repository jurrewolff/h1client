# h1client

A personal project to learn the C programming language, and it's development context.

## Goal

The goal of "h1client" is a pure C application, which is able to send basic HTTP requests, while utilizing as little
libraries as possible, forcing me to develop these components myself.

## Features

- CLI argument parsing
- URI parsing library
- TCP socket connections
- HTTP calls
    - GET
    - HEAD (todo)
- Logging library
- Testing (using Throw The Switch's ["Unity"](https://github.com/ThrowTheSwitch/Unity))

## Installation

1. Install Unity test framework
    - [Recommended way](https://github.com/ThrowTheSwitch/Unity/issues/302#issuecomment-508945504) (by "dontlaugh")
    - Note: Only follow section "Building Unity"
    - Note: Skip last "cp" command.
2. Clone this project
3. Use cmake and make to build binaries:

```bash
cmake -S ./ -B ./build
cd build
make
```

4. Run it:

```bash
./build/bin/h1client
```

Running "h1client_test" binary runs unit-tests.
