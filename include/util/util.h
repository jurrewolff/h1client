//
// Created by jurrew on 9/13/21.
//

#ifndef H1CLIENT_UTIL_H
#define H1CLIENT_UTIL_H

#include "config/cfg.h"
#include "log/log.h"

unsigned int StrArrayContains(char **arr, unsigned int arrLen, const char *target);

int StrHasPrefix(const char *prefix, const char *str);

char *logLevelEnumToStr(enum logLevels level);

int strToLogLevelEnum(char *levelStr, enum logLevels *levelEnum);

#endif //H1CLIENT_UTIL_H
