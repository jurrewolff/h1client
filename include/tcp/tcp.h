//
// Created by jurrew on 9/15/21.
//

#ifndef H1CLIENT_TCP_H
#define H1CLIENT_TCP_H

int socketConnect(char *address, int Port);

int socketSend(int socketFD, const char *data);

int socketReceive(int socketFD, char **respData);

#endif //H1CLIENT_TCP_H
