//
// Created by jurrew on 9/16/21.
//

#ifndef H1CLIENT_URI_H
#define H1CLIENT_URI_H

#include "log/log.h"

extern int (*test_logF_uri)(struct logger *l, enum logLevels level, char *format, ...);

struct uri {
    char scheme[24];
    char userInfo[1024];
    char host[1024];
    char port[6];
    char path[1024];
    char query[1024];
    char fragment[1024];
};

int parse(char *uri, struct uri *parsedURI);

#endif //H1CLIENT_URI_H
