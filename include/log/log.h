//
// Created by jurrew on 10/7/21.
//

#ifndef H1CLIENT_LOG_H
#define H1CLIENT_LOG_H

#include <stdio.h>

enum logLevels {
    OFF = 0,
    FATAL = 1,
    ERROR = 2,
    INFO = 3,
    DEBUG = 4,
};

struct logger;

struct logger* getLogger(void);

void setLogLevel(struct logger *self, enum logLevels level);

void setLogOutput(struct logger *self, FILE *f);

int logF(struct logger *self, enum logLevels level, char *format, ...);

#endif //H1CLIENT_LOG_H
