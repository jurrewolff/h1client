//
// Created by jurrew on 9/13/21.
//

#ifndef H1CLIENT_PROCESS_ARGS_H
#define H1CLIENT_PROCESS_ARGS_H

#include "config/cfg.h"

int ProcessArgs(int argc, char *argv[], struct ReqConf *cfg);

#endif //H1CLIENT_PROCESS_ARGS_H
