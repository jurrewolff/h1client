//
// Created by jurrew on 9/21/21.
//

#ifndef H1CLIENT_HTTP_H
#define H1CLIENT_HTTP_H

#include "uri/uri.h"

int get(struct uri *parsedURI, char **response);

#endif //H1CLIENT_HTTP_H
