//
// Created by jurrew on 9/13/21.
//

#ifndef H1CLIENT_CFG_H
#define H1CLIENT_CFG_H

#include "log/log.h"

extern char *ReqArr[2];

struct StrList {
    char *Data;
    struct StrList *Next;
};

struct ReqHeader {
    char *FieldName;
    char *FieldVal;
};

struct ReqConf {
    enum logLevels logLevel;
    char *logFile;
    struct StrList *TargetURIs;
    char *ReqMethod; // TODO - Implement method selection (HEAD or GET).
    struct ReqHeader reqHeaders[]; // TODO - Implement custom header support.
};

#endif //H1CLIENT_CFG_H
