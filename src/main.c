#include <stdio.h>
#include <uri/uri.h>
#include <malloc.h>
#include <http/http.h>
#include <string.h>
#include <log/log.h>
#include <util/util.h>

#include "config/cfg.h"
#include "args/process_args.h"

int main(int argc, char *argv[]) {
    struct logger *l = getLogger();
    int err;
    FILE *logFile = NULL;

    struct ReqConf cfg = {0};
    err = ProcessArgs(argc, argv, &cfg);
    if (err < 0) {
        logF(l, FATAL, "Processing arguments failed.\n");
        return 1;
    }

    if (strcmp(logLevelEnumToStr(cfg.logLevel), "") != 0) {
        setLogLevel(l, cfg.logLevel);
    }

    if (cfg.logFile != NULL) {
        logFile = fopen(cfg.logFile, "a");
        setLogOutput(l, logFile);
    }
    
    struct StrList *currentURI = cfg.TargetURIs;
    while (1) {
        struct uri *parsedURI = (struct uri *) calloc(1, sizeof(struct uri));
        if (parsedURI == NULL) {
            logF(l, FATAL, "Failed to allocate memory for parsed URI object.\n");
            return -1;
        }

        err = parse(currentURI->Data, parsedURI);
        if (err) {
            logF(l, ERROR, "Failed to parse URI.\n");
            return -1;
        }

        char *response = (char *) calloc(1, sizeof(char *) + 1);
        get(parsedURI, &response);
        free(parsedURI);

        printf("%s", response);
        free(response);

        if (currentURI->Next) {
            currentURI = currentURI->Next;
        } else {
            break;
        }
    }

    currentURI = NULL;
    while ((currentURI = cfg.TargetURIs) != NULL) {
        cfg.TargetURIs = cfg.TargetURIs->Next;
        free(currentURI);
    }

    if (cfg.logFile) {
        fclose(logFile);
    }

    return 0;
}
