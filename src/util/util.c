//
// Created by jurrew on 9/13/21.
//

#include <string.h>

#include "util/util.h"
#include "log/log.h"

unsigned int StrArrayContains(char **arr, unsigned int arrLen, const char *target) {
    for (int i = 0; i < arrLen; ++i) {
        if (strcmp(arr[i], target) == 0) {
            return 1;
        }
    }
    return 0;
}

int StrHasPrefix(const char *prefix, const char *str) {
    return strncmp(prefix, str, strlen(prefix)) == 0;
}

char *logLevelEnumToStr(enum logLevels level) {
    switch (level) {
        case OFF:
            return "OFF";
        case FATAL:
            return "FATAL";
        case ERROR:
            return "ERROR";
        case INFO:
            return "INFO";
        case DEBUG:
            return "DEBUG";
        default:
            return "";
    }
}

int strToLogLevelEnum(char *levelStr, enum logLevels *levelEnum) {
    if (strcmp(levelStr, "OFF") == 0) {
        *levelEnum = OFF;
    } else if (strcmp(levelStr, "FATAL") == 0) {
        *levelEnum = FATAL;
    } else if (strcmp(levelStr, "ERROR") == 0) {
        *levelEnum = ERROR;
    } else if (strcmp(levelStr, "INFO") == 0) {
        *levelEnum = INFO;
    } else if (strcmp(levelStr, "DEBUG") == 0) {
        *levelEnum = DEBUG;
    } else {
        return -1;
    }

    return 0;
}