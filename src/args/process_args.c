//
// Created by jurrew on 9/13/21.
//

#include <getopt.h>
#include <string.h>
#include <malloc.h>

#include "config/cfg.h"
#include "util/util.h"
#include "log/log.h"

int ProcessArgs(int argc, char *argv[], struct ReqConf *cfg) {
    struct logger *l = getLogger();
    unsigned int index;
    int c;
    int err;

    while (1) {
        static struct option long_options[] =
                {
                        {"method",   required_argument, 0, 'm'},
                        {"loglevel", required_argument, 0, 'l'},
                        {"logfile",  required_argument, 0, 'f'},
                };

        int option_index = 0; // getopt_long stores the option index here.
        c = getopt_long(argc, argv, "m:l:f:", long_options, &option_index);

        if (c == -1) // Detect the end of the options.
            break;

        enum logLevels levelEnum;
        switch (c) {
            case 0:
                break; // Used for flags
            case 'm':
                if (StrArrayContains(ReqArr, (sizeof ReqArr / sizeof ReqArr[0]), optarg)) {
                    cfg->ReqMethod = optarg;
                } else {
                    logF(l, INFO, "Unknown argument for option -%c.\n", c);
                    return -1;
                }
                break;

            case 'l':
                err = strToLogLevelEnum(optarg, &levelEnum);
                if (err < 0) {
                    logF(l, INFO, "Unknown argument for option -%c.\n", c);
                    return -1;
                }
                cfg->logLevel = levelEnum;
                break;

            case 'f':
                cfg->logFile = optarg;
                break;

            case '?':
                break; // getopt_long already printed an error message.

            default:
                logF(l, FATAL, "Error parsing options.");
                return -1;
        }
    }

    struct StrList *urlListHead = (struct StrList *) malloc(sizeof(struct StrList));
    struct StrList *urlListCurrent;
    for (index = optind; index < argc; index++) {
        if (index == optind) { // First loop
            urlListHead->Data = argv[index];
            urlListHead->Next = NULL;
            urlListCurrent = urlListHead;
            continue;
        }

        // Allocate memory for new list item
        struct StrList *newItem = (struct StrList *) malloc(sizeof(struct StrList));
        if (newItem == NULL) {
            logF(l, ERROR, "Failed to allocate memory memory for new list item.\n");
            return -1;
        }

        // Set data of item
        newItem->Data = argv[index];
        newItem->Next = NULL;

        urlListCurrent->Next = newItem;
        urlListCurrent = urlListCurrent->Next;
    }

    if (!urlListHead->Data) {
        logF(l, INFO, "Please provide at least one URL.\n");
        return -1;
    }

    cfg->TargetURIs = urlListHead;

    return 0;
}
