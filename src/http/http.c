//
// Created by jurrew on 9/21/21.
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "log/log.h"
#include "http/http.h"
#include "tcp/tcp.h"
#include "uri/uri.h"

const char *formatGET = "GET %s HTTP/1.0\n"
                        "Host: %s\n"
                        "User-Agent: PotatoEngine/1337\n"
                        "Connection: close\n\n";

int get(struct uri *parsedURI, char **response) {
    struct logger *l = getLogger();

    char *request = (char *) malloc(
            strlen(formatGET) +
            strlen(parsedURI->path) +
            strlen(parsedURI->host) + 1
    );

    int err = sprintf(
            request,
            formatGET,
            parsedURI->path,
            parsedURI->host
    );
    if (err < 0) {
        logF(l, ERROR, "Failed to format the http request.");
        free(request);
        return -1;
    }

    // Create socket connection.
    int socketFD;

    char *endPtr;
    int port = (int) strtol(parsedURI->port, &endPtr, 10);

    if (port == 0 && strcmp(endPtr, "") != 0) {
        logF(l, ERROR, "Port isn't numerical.\n");
        return -1;
    }

    socketFD = socketConnect(parsedURI->host, port);
    if (socketFD < 0) {
        logF(l, ERROR, "Error connecting with socket.\n");
        close(socketFD);
        free(request);
        return -1;
    }

    // Send request over socket
    if (socketSend(socketFD, request) < 0) {
        logF(l, ERROR, "Error sending request over socket.\n");
        close(socketFD);
        free(request);
        return -1;
    }
    free(request);

    // Receive response
    if (socketReceive(socketFD, response) < 0) {
        logF(l, ERROR, "Error receiving response from socket.\n");
        close(socketFD);
    }

    // Finally, close the used socket.
    if (close(socketFD) < 0) {
        logF(l, ERROR, "Failed to close tcp socket.");
        return -1;
    }

    return 0;
}
