//
// Created by jurrew on 10/7/21.
//

#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <util/util.h>
#include "log/log.h"

struct logger {
    unsigned int initialized;
    enum logLevels level;
    FILE *output;
};

struct logger logger;

// Freeing memory of 'logger' is caller's responsibility.
struct logger *getLogger(void) {
    if (logger.initialized) {
        return &logger;
    }

    logger.level = INFO;
    logger.output = stderr;
    logger.initialized = 1;

    return &logger;
}

void setLogLevel(struct logger *self, enum logLevels level) {
    self->level = level;
}

void setLogOutput(struct logger *self, FILE *f) {
    self->output = f;
}

int logF(struct logger *self, enum logLevels level, char *format, ...) {
    va_list vaList;
    int charsWritten = 0;

    time_t t = time(NULL);
    if (t < 0) {
        fprintf(stderr, "Failed to get time for log message.");
        t = 0;
    }

    const unsigned int bufSiz = 128;
    char *logPrefix = (char *) calloc(bufSiz, sizeof(char));
    int charCount;
    charCount = snprintf(logPrefix, bufSiz, "%s [%ld]: %s", logLevelEnumToStr(level), t, format);

    if (charCount <= 0 || charCount > bufSiz) {
        logPrefix = realloc(logPrefix, charCount + 1);
        memset(logPrefix, 0, strlen(logPrefix));
        snprintf(logPrefix, charCount + 1, "%s [%ld]: %s", logLevelEnumToStr(self->level), t, format);
    }

    if (level <= self->level) {
        va_start(vaList, format);
        charsWritten = vfprintf(self->output, logPrefix, vaList);
        va_end(vaList);
    }

    free(logPrefix);
    logPrefix = NULL;

    return charsWritten;
}
