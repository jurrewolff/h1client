//
// Created by jurrew on 9/15/21.
//

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>

#include "tcp/tcp.h"
#include "log/log.h"

int socketConnect(char *address, int port) {
    struct logger *l = getLogger();
    int socketFD;
    struct sockaddr_in servAddr;

    socketFD = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socketFD < 0) {
        logF(l, ERROR, "Failed to create tcp socket.\n");
        return -1;
    }

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr(address);
    servAddr.sin_port = htons(port);

    if (connect(socketFD, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
        logF(l, ERROR, "Failed to connect with server [%s:%d].\n", address, port);
        return -1;
    }

    return socketFD;
}

int socketSend(int socketFD, const char *data) {
    struct logger *l = getLogger();
    if (write(socketFD, data, strlen(data)) < 0) {
        logF(l, ERROR, "Failed to send tcp data.\n");
        return -1;
    }
    return 0;
}

int socketReceive(int socketFD, char **respData) {
    struct logger *l = getLogger();
    char buf[BUFSIZ] = {0};
    ssize_t bytesRead;

    while ((bytesRead = read(socketFD, buf, BUFSIZ)) > 0) {
        *respData = realloc(*respData, strlen(*respData) + sizeof buf + 1);
        if (*respData == NULL) {
            logF(l, ERROR, "Failed to increase memory size for tcp response.\n");
            return -1;
        }

        strcat(*respData, buf);
        memset(buf, 0x00, sizeof buf); // Last loop might partially fill buf, causing old data to be appended to end.
    }

    if (bytesRead < 0) {
        logF(l, ERROR, "Failed to read tcp data.\n");
        return -1;
    }

    return 0;
}
