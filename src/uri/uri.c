//
// Created by jurrew on 9/16/21.
//

#include <string.h>
#include <util/util.h>
#include <malloc.h>
#include <ctype.h>

#include "uri/uri.h"

#ifdef TEST
#define LOGF logF
#else
#define LOGF test_logF_uri
#endif

int (*test_logF_uri)(struct logger *l, enum logLevels level, char *format, ...) = logF;

struct strExtractRes {
    char *result;
    char lastDelim;
};

// Freeing memory of 'strExtractRes.result' is callers responsibility.
struct strExtractRes extractStringUntil(char **str, const char delimiters[], unsigned int delimLen) {
    char *postDelimStr;
    struct strExtractRes ser = {0};
    ser.result = calloc(strlen(*str) + 1, sizeof(char));
    char *strVal = *str;

    for (int i = 0; i < delimLen; ++i) {
        postDelimStr = strchr(*str, delimiters[i]);
        if (!postDelimStr) {
            continue;
        }

        for (int j = 0; j < strlen(*str); ++j) {
            if (i == strlen(*str)) { return ser; }

            char delimStr[2] = {delimiters[i], '\0'};
            if (strVal[j] == delimiters[i]) {
                ser.lastDelim = delimiters[i];
                strtok_r(NULL, delimStr, str);
                goto parseHostEnd; // 😎
            }
            ser.result[j] = strVal[j];
        }
    }
    parseHostEnd:
    return ser;
}

int parse(char *uri, struct uri *parsedURI) {
    struct logger *l = getLogger();
    char *remaining = uri;
    char *token;

    // Get scheme
    strcpy(parsedURI->scheme, strtok_r(uri, ":", &remaining));

    // Strip slashes
    if (StrHasPrefix("//", remaining)) {
        size_t strLen = strlen("//");
        remaining = strncmp(remaining, "//", strLen) ? remaining : remaining + strLen;
    }

    // Get userInfo
    token = strtok_r(NULL, "@", &remaining);
    if (strcmp(remaining, "") != 0) {
        strcpy(parsedURI->userInfo, token);
    } else {
        remaining = token;
    }

    // Get host
    const char postHostIDs[5] = {':', '/', '?', '#', '\0'};
    struct strExtractRes ser = extractStringUntil(&remaining, postHostIDs, sizeof postHostIDs);
    strcpy(parsedURI->host, ser.result);
    free(ser.result);
    ser.result = NULL;

    // Get port
    if (ser.lastDelim == ':') {
        for (int i = 0; i <= strlen(remaining); ++i) {
            if (i == strlen(remaining)) { return 0; }

            char remainingStr[2] = {remaining[i], '\0'};
            if (!isdigit(remaining[i]) && strcmp(parsedURI->port, "") <= 0) {
                LOGF(l, ERROR, "Port not specified at supposed location.");
                return -1;
            } else if (!isdigit(remaining[i])) {
                ser.lastDelim = remaining[i];
                strtok_r(NULL, remainingStr, &remaining);
                break;
            }
            parsedURI->port[i] = remaining[i];
        }
    }

    if (strcmp(parsedURI->port, "") == 0) {
        strcpy(parsedURI->port, "80");
    }

    // Get Path
    strcpy(parsedURI->path, "/");
    if (ser.lastDelim == '/') {
        const char postPathIDs[3] = {'?', '#', '\0'};
        ser = extractStringUntil(&remaining, postPathIDs, sizeof postPathIDs);
        parsedURI->path[0] = '/';
        strcat(parsedURI->path, ser.result);
        free(ser.result);
        ser.result = NULL;
    }

    // Get Query
    if (ser.lastDelim == '?') {
        char postQueryIDs[2] = {'#', '\0'};
        ser = extractStringUntil(&remaining, postQueryIDs, sizeof postQueryIDs);
        strcpy(parsedURI->query, ser.result);
        free(ser.result);
        ser.result = NULL;
    }

    // Get Fragment
    if (ser.lastDelim == '#') {
        strcpy(parsedURI->fragment, remaining);
    }

    return 0;
}
