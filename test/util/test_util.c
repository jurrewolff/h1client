//
// Created by jurrew on 9/24/21.
//

#define TEST

#include <unity/unity.h>

#include "util/util.h"

void setUp(void) {}

void tearDown(void) {}

void test_StrArrayContains_should_ReturnCorrectBool(void) {
    char *strArray[3][7] = {"String1", "String2", "String3"};
    unsigned int res;

    res = StrArrayContains(*strArray, 3, "String3");
    TEST_ASSERT_TRUE(res);

    res = StrArrayContains(*strArray, 3, "String4");
    TEST_ASSERT_FALSE(res);
}
