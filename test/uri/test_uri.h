//
// Created by jurrew on 9/28/21.
//

#ifndef H1CLIENT_TEST_URI_H
#define H1CLIENT_TEST_URI_H

void test_parse_should_ParseFullURI(void);

void test_parse_should_ParseSimpleURI(void);

void test_parse_should_ParseURIEndingWithQuery(void);

void test_parse_should_ParseURIEndingWithFragment(void);

void test_parse_should_not_ParseURIWithPortIndicatorButWithoutPort(void);

void test_parse_should_SetPortTo80WhenNotSpecified(void);

#endif //H1CLIENT_TEST_URI_H
