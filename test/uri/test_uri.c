//
// Created by jurrew on 9/28/21.
//

#include <unity/unity.h>
#include <string.h>

#include "uri/uri.h"
#include "log/log.h"

int mock_logF_uri(struct logger *l, enum logLevels level, char *format, ...) {
    return 0;
}

void test_parse_should_ParseFullURI(void) {
    int err;
    char fullUri[] = "https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top";
    struct uri parsedURI = {0};

    err = parse(fullUri, &parsedURI);

    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_STRING("https", parsedURI.scheme);
    TEST_ASSERT_EQUAL_STRING("john.doe", parsedURI.userInfo);
    TEST_ASSERT_EQUAL_STRING("www.example.com", parsedURI.host);
    TEST_ASSERT_EQUAL_STRING("123", parsedURI.port);
    TEST_ASSERT_EQUAL_STRING("/forum/questions/", parsedURI.path);
    TEST_ASSERT_EQUAL_STRING("tag=networking&order=newest", parsedURI.query);
    TEST_ASSERT_EQUAL_STRING("top", parsedURI.fragment);
}

void test_parse_should_ParseSimpleURI(void) {
    int err;
    char simpleURI[] = "http://www.example.com";
    struct uri parsedURI = {0};

    err = parse(simpleURI, &parsedURI);

    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_STRING("http", parsedURI.scheme);
    TEST_ASSERT_EQUAL_STRING("www.example.com", parsedURI.host);
}

void test_parse_should_ParseURIEndingWithQuery(void) {
    int err;
    char URIEndsWithQuery[] = "http://www.example.com?tag=networking&order=newest";
    struct uri parsedURI = {0};

    err = parse(URIEndsWithQuery, &parsedURI);

    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_STRING("http", parsedURI.scheme);
    TEST_ASSERT_EQUAL_STRING("www.example.com", parsedURI.host);
    TEST_ASSERT_EQUAL_STRING("tag=networking&order=newest", parsedURI.query);
}

void test_parse_should_ParseURIEndingWithFragment(void) {
    int err;
    char URIEndsWithFragment[] = "http://www.example.com#top";
    struct uri parsedURI = {0};

    err = parse(URIEndsWithFragment, &parsedURI);

    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_STRING("http", parsedURI.scheme);
    TEST_ASSERT_EQUAL_STRING("www.example.com", parsedURI.host);
    TEST_ASSERT_EQUAL_STRING("top", parsedURI.fragment);
}

void test_parse_should_not_ParseURIWithPortIndicatorButWithoutPort(void) {
    test_logF_uri = mock_logF_uri;

    int err;
    char URIEndsWithFragment[] = "http://www.example.com:/forum/questions/";
    struct uri parsedURI = {0};

    err = parse(URIEndsWithFragment, &parsedURI);

    TEST_ASSERT_EQUAL_INT(-1, err);
}

void test_parse_should_SetPortTo80WhenNotSpecified(void) {
    int err;
    char URIEndsWithFragment[] = "http://www.example.com/forum/questions/";
    struct uri parsedURI = {0};

    err = parse(URIEndsWithFragment, &parsedURI);

    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_STRING("80", parsedURI.port);
    TEST_ASSERT_EQUAL_STRING("http", parsedURI.scheme);
    TEST_ASSERT_EQUAL_STRING("www.example.com", parsedURI.host);
    TEST_ASSERT_EQUAL_STRING("/forum/questions/", parsedURI.path);
}
