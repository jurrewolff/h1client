//
// Created by jurrew on 9/28/21.
//

#include <unity/unity.h>

#include "../test/util/test_util.h"
#include "../test/uri/test_uri.h"

int main(void) {
    UNITY_BEGIN();

    // Util
    RUN_TEST(test_StrArrayContains_should_ReturnCorrectBool);

    // Uri
    RUN_TEST(test_parse_should_ParseFullURI);
    RUN_TEST(test_parse_should_ParseSimpleURI);
    RUN_TEST(test_parse_should_ParseURIEndingWithQuery);
    RUN_TEST(test_parse_should_ParseURIEndingWithFragment);
    RUN_TEST(test_parse_should_not_ParseURIWithPortIndicatorButWithoutPort);
    RUN_TEST(test_parse_should_SetPortTo80WhenNotSpecified);

    return UNITY_END();
}
